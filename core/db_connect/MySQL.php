<?php

namespace Core\DataBase;

use Exception;

class MySQL
{
    public static function connect()
    {
        $db_conf = include 'config/db_conf.php';
        if (!isset($db_conf['db_name']) || !isset($db_conf['user']) || !isset($db_conf['password'])) {
            throw new Exception('Error: No correct db connection config, edit file config/db_conf.php');
        }

        $link = mysqli_connect($db_conf['db_route'], $db_conf['user'], $db_conf['password']);

        if (!$link) {
            throw new Exception(mysql_error());
        }

        $select = mysqli_query($link, 'USE ' . $db_conf['db_name']);

        if (!$select) {
            throw new Exception('Error: failed to connect to database ' . $db_conf['db_name']);
        }

        return $link;
    }
}



