<?php


namespace Core;

use Psr\Http\Message\ServerRequestInterface as Request;

class CheckParams
{
    static function run(Request $request, array $params)
    {
        foreach ($params as $param){
            if(!isset($request->getQueryParams()[$param])){
                throw new \Exception('Error: parameter "' . $param . '" is required');
            }
        }
    }
}