<?php


namespace Core\Security;


use Service\UserService;

class Auth
{
    public static function checkUserRights(string $api_key):bool
    {
        if(self::getRights($api_key) != ''){
            return true;
        }

        return false;
    }

    public static function checkRootRights(string $api_key):bool
    {
        if(self::getRights($api_key) == 'root'){
            return true;
        }

        return false;
    }

    public static function getRights(string $api_key):string
    {
        $user_service = new UserService();
        $user = $user_service->get(['api_key'=>$api_key])[0];

        return isset($user['access_level']) ? $user['access_level'] : '';
    }
}