<?php

class Autoload
{
    private static $include_file_dirs_arr = ['core', 'src/Service'];

    //по нормальному здесь не должно быть статик а должна быть еще 1 функция добавления file_dir но у нас тут минимализм
    public static function load(){
        foreach (self::$include_file_dirs_arr as $include_file_dir_arr){
            self::require_php_files_in_path($include_file_dir_arr);
        }
    }

    private static function require_php_files_in_path($path){
        $dir_files = scandir($path);
        foreach ($dir_files as $key => $item){
            if($item != '.' && $item != '..'){
                if(strripos($item, '.php') === false){
                    self::require_php_files_in_path($path . '/' . $item);
                } else{
                    require $path . '/' . $item;
                }
            }
        }
    }
}