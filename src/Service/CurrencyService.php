<?php
namespace Service;

use Core\DataBase\MySQL;

class CurrencyService
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = MySQL::connect();
    }

    public function get(array $params = null, int $count = null, int $page = null)
    {
        $query = 'SELECT * FROM Currency';
        if($params){
            $field = mysqli_real_escape_string($this->mysql, key($params));
            $value = mysqli_real_escape_string($this->mysql, $params[$field]);
            $query = 'SELECT * FROM Currency WHERE `' . $field . '` = "' . $value . '"';
        }

        if(!is_null($count) && !is_null($page)){
            $page *= $count;

            $query .= ' LIMIT ' . $page . ', ' . $count;
        }

        $result = mysqli_query($this->mysql, $query);

        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function add(string $name, int $rate)
    {
        $name = mysqli_real_escape_string($this->mysql, $name);

        $query = 'INSERT INTO Currency (name, rate) VALUES ("' . $name . '", "' . $rate . '")';
        $result = mysqli_query($this->mysql, $query);

        if(!$result){
            throw new \Exception(mysqli_error($this->mysql));
        }
    }

    public function upgradeAny(array $params)
    {
        $query = 'INSERT INTO Currency (name, rate) VALUES ';
        foreach ($params as $param){
            $query .= '("' .
                (isset($param['name']) ? mysqli_real_escape_string($this->mysql, $param['name']) : 'NULL') . '", "' .
                (isset($param['rate']) ? mysqli_real_escape_string($this->mysql, $param['rate']) : 'NULL') .
            '"),';
        }
        $query = substr($query, 0, -1);

        $query .= ' ON DUPLICATE KEY UPDATE `rate` = VALUES(`rate`)';

        $result = mysqli_query($this->mysql, $query);

        if(!$result){
            throw new \Exception(mysqli_error($this->mysql));
        }
    }

    public function delete()
    {

    }
}