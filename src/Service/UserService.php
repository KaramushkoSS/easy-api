<?php

namespace Service;

use Core\DataBase\MySQL;

class UserService
{
    private $mysql;

    public function __construct()
    {
        $this->mysql = MySQL::connect();
    }

    public function get(array $params = null)
    {
        $query = 'SELECT * FROM User';
        if($params){
            $field = mysqli_real_escape_string($this->mysql, key($params));
            $value = mysqli_real_escape_string($this->mysql, $params[$field]);
            $query = 'SELECT * FROM User WHERE `' . $field . '` = "' . $value . '"';
        }
        $result = mysqli_query($this->mysql, $query);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function add(string $name, string $access_lvl)
    {
        $name = mysqli_real_escape_string($this->mysql, $name);
        $access_lvl = mysqli_real_escape_string($this->mysql, $access_lvl);

        $query = 'INSERT INTO User (name, api_key, access_level) VALUES ("' . $name . '", "' . $this->keyGenerate() . '", "' . $access_lvl . '")';
        $result = mysqli_query($this->mysql, $query);

        if(!$result){
            throw new \Exception(mysqli_error($this->mysql));
        }
    }

    public function update()
    {

    }

    public function remove()
    {

    }


    private function keyGenerate($keyLength = 16)
    {
        $apiKey = '';
        for ($i = 0; $i < $keyLength; $i++) {
            $apiKey .= chr(mt_rand(33, 126)); //символ из ASCII-table
        }
        $apiKey = md5($apiKey);

        return $apiKey;
    }

}