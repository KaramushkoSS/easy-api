<?php

use Core\DataBase\MySQL;
use Service\UserService;

class DefaultDBConstructorCommand
{
    public static function generate()
    {
        try{
            self::createUserTable();
            self::createDefaultUser();
            self::createCurrencyTable();
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }

    public static function createUserTable()
    {
        try{
            $mysql = MySQL::connect();
            $query = mysqli_query($mysql, 'CREATE TABLE User (
            id INT PRIMARY KEY AUTO_INCREMENT, 
            name VARCHAR(255) UNIQUE NOT NULL, 
            api_key VARCHAR(255) UNIQUE NOT NULL, 
            access_level VARCHAR(255) NOT NULL
        )');
            if(!$query){
                echo 'Error create User table' . PHP_EOL;
            } else {
                echo 'User table create' . PHP_EOL;
            }
            mysqli_close($mysql);
        } catch (Exception $e) {
            echo 'Error create User table: ' . $e->getMessage() . PHP_EOL;
        }

    }

    public static function createDefaultUser()
    {
        try{
            $users = [
                [
                    'name' => 'admin',
                    'access_level' => 'root'
                ],
                [
                    'name' => 'default',
                    'access_level' => 'user'
                ]
            ];

            $service = new UserService();
            foreach ($users as $user){
                $service->add($user['name'], $user['access_level']);
            }

            echo 'Default users added' . PHP_EOL;
        } catch (Exception $e) {
            echo 'Error default users added: ' . $e->getMessage() . PHP_EOL;
        }

    }

    public static function createCurrencyTable()
    {
        try{
            $mysql = MySQL::connect();
            $query = mysqli_query($mysql, 'CREATE TABLE Currency (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) UNIQUE NOT NULL, rate FLOAT(4) NOT NULL)');
            if(!$query){
                echo 'Error create Currency table' . PHP_EOL;
            } else{
                echo 'Currency table create' . PHP_EOL;
            }
            mysqli_close($mysql);
        } catch (Exception $e) {
            echo 'Error create Currency table: ' . $e->getMessage() . PHP_EOL;
        }

    }

}