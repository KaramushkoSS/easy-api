<?php

use Core\DataBase\MySQL;
use Service\CurrencyService;

class CurrencyUpdateCommand
{
    private $mysql;

    public function __construct()
    {
        try {
            $this->mysql = MySQL::connect();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateFromUrl($url)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $responce = curl_exec($ch);
            self::loadXml($responce);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    public function updateFromXML($name)
    {
        try{
            $xml = file_get_contents('document/' . $name);
            self::loadXml($xml);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function loadXml(string $xml)
    {
        $xml = new SimpleXMLElement($xml);
        $currencies = [];
        for ($i = 0; $i < $xml->Valute->count(); $i++) {
            $currencies[] = [
                'name' => $xml->Valute[$i]->Name,
                'rate' => str_replace(',', '.', $xml->Valute[$i]->Value) / $xml->Valute[$i]->Nominal
            ];
        }

        $service = new CurrencyService();
        $service->upgradeAny($currencies);
    }
}