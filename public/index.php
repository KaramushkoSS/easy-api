<?php

use Core\CheckParams;
use Core\Security\Auth;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Service\CurrencyService;
use Slim\Factory\AppFactory;


require __DIR__ . '/../vendor/autoload.php';
require_once 'autoload/Autoload.php';

Autoload::load();

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) {
    $response->getBody()->write('<head><script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script></head>');
    return $response;
});

$app->get('/currency', function (Request $request, Response $response, $args) {
    try{
        CheckParams::run($request, ['api_key', 'id']);
        $params = $request->getQueryParams();
        if(Auth::checkUserRights($params['api_key'])){
            $currency_service = new CurrencyService();
            $currency = $currency_service->get(['id' => $params['id']]);
            if(!empty($currency)){
                $response->getBody()->write(json_encode(['success' => true, 'result' => $currency[0]]));
            } else{
                $response->getBody()->write(json_encode(['success' => false, 'error' => 'Error: currency with id "' . $params['id'] . '" not found']));
            }
        } else {
            $response->getBody()->write(json_encode(['success' => false, 'error' => 'Error: Api key is not valid']));
        }
    }catch (Exception $e){
        $response->getBody()->write(json_encode(['success' => false, 'error' => $e->getMessage()]));
    }

    return $response;
});

$app->get('/currencies', function (Request $request, Response $response, $args) {
    try {
        CheckParams::run($request, ['api_key']);
        $params = $request->getQueryParams();
        if(Auth::checkUserRights($params['api_key'])){
            $currency_service = new CurrencyService();
            if(isset($params['count']) && isset($params['page'])){
                $currency = $currency_service->get(null, $params['count'], $params['page']);
            } else {
                $currency = $currency_service->get();
            }
            $response->getBody()->write(json_encode(['success' => true, 'result' => $currency]));
        }else {
            $response->getBody()->write(json_encode(['success' => false, 'error' => 'Error: Api key is not valid']));
        }
    } catch (Exception $e){
        $response->getBody()->write(json_encode(['success' => false, 'error' => $e->getMessage()]));
    }

    return $response;
});

$app->run();